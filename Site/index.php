<!DOCTYPE html>
<html>
<head>
	<title>Lecoffre Antoine portfolio</title> <!-- titre affiché sur l'onglet -->
	<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css"/> <!-- CSS pour le parallax -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> <!-- CSS bootstrap -->
  <link rel="stylesheet" type="text/css" href="parallax.css"> <!-- CSS des slides avec l'animation fade in -->
  <link rel="stylesheet" type="text/css" href="style.css"> <!-- CSS restant -->



</head>
<body> <!-- corps de la page -->

	<nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light"> <!-- Navbar bootstrap -->
    <a class="navbar-brand" href="#">Menu</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown" >
      
      <ul class="navbar-nav">
        
        <li class="nav-item dropdown"style="margin-left: 120px; margin-right: 150px;"> <!-- menu déroulant "qui suis-je" -->
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Qui suis-je </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="#Presentation">Présentation</a>
            <a class="dropdown-item" href="#Compétences">Compétences</a>
            <a class="dropdown-item" href="#Hobbies">Hobbies</a>
            <a class="dropdown-item" href="#Projets">Projets</a>
          </div>
        </li>
        
        <li class="nav-item dropdown" style="margin-left: 120px; margin-right: 150px;"> <!-- menu déroulant "pro" -->
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Professionnel</a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="#Experiences">Expériences</a>
            <a class="dropdown-item" href="#Recommandations">Recommandations</a>
          </div>
        </li>

        <li class="nav-item active" style="margin-left: 120px; margin-right: 150px;"> <!-- lien vers contact -->
          <a class="nav-link" href="#Contact">Contact<span class="sr-only">(current)</span></a>
        </li>

        <li class="nav-item dropdown" style="margin-left: 120px; margin-right: 10px;"> <!-- menu déroulant "pro" -->
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Langues</a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="index.html">Français</a>
            <a class="dropdown-item" href="english.html">English</a>
          </div>
        </li>

      </ul>
    </div>
  </nav>

  <div id="slide1"> <!-- Slide 1 -->
	
	   <div class="slide_inside">
    		<h2 id="Presentation">Lecoffre Antoine</h2>
    		
        <img src="image/logo.jpg" id="monlogo">
    		
        <img src="image/0.png" id="pplinkedin">
    		
        <img src="image/logo.png" id="logoecole">
        
        <p id="phraseIntro">Etudiant en 1ère année en bachelor informatique à Campus Academy Rennes</p>

        <p id="Presentation">Présentation</p>

        <p id="phraseBienvenue">Je vous souhaite la bienvenue sur mon site. Ceci est mon portfolio. </p> 
        
        <p id="phrasePresentation"> Je m'appelle Antoine, j'ai 18 ans et je suis un étudiant en informatique à Rennes, si j'ai choisi la voie de l'informatique, c'est parce que c'est un domaine d'avenir et surtout une de mes passions.</p> 
        
        <p id="objectifFuturs"> Mes objectifs post-études sont de travailler dans la cybersécurité ou le développement web.</p>

     </div>

  </div>

  <div id="slide2">
  	
  	<div class="slide_inside">
  		<h2 id="Compétences">Compétences</h2>
      <table class="table table-dark">
        
          <thead>
            <tr>
              <th scope="col">Pas encore commencé</th>
              <th scope="col">En cours</th>
              <th scope="col">Bonne maîtrise</th>
              </tr>
          </thead>

        <tbody> 
          <tr>
            <td> <!-- les images pour le "pas encore commencé"" -->
              <img id="nodepng" src="image/node.png"> <br>
              <img id="laravelpng" src="image/laravel.png"><br>
            	<img id="jquerypng" src="image/jquery.png">
            </td>
            
            <td> <!-- les images pour le "en cours" -->
                <img id="javascriptpng" src="image/javascript.png">
                <img id="phppng" src="image/php.png"> 
                <br>
                <img id="postgrepng" src="image/postgre.png">
                <img id="sqlpng" src="image/sql.png">
            </td>
            
            <td> <!-- les images pour le "bonne maîtrise" -->
              <img id="htmlpng" src="image/html.png" style>
              <img id="csspng" src="image/css.png"> <br>
              <img id="bootstrappng" src="image/bootstrap.png">
            </td>

          </tr>
       </tbody>
     </table>
    
     <table class="table table-dark"> <!-- 2em table pour dire avec quoi je travail comme outils -->
      <thead>
        <tr>
          <th scope="col">Mes outils :</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
          <img id="sublimepng" src="image/sublime.png">
          <img id="webstormpng" src="image/WebStorm.png">
          <img id="officepng" src="image/office.jpg">
        </td>
        </tr>
      </tbody>
     </table>
  	</div>

  </div>

  <div id="slide3"> <!-- Slide 3 -->
  	
  	<div class="slide_inside">
  		<h2 id="Hobbies">Hobbies</h2>
      
      <img id="natationpng" src="image/natation.png"> <br> <!-- les images qui sont dans ma slide -->
      
      <img id="karatepng" src="image/karate.png">
      
      <img id="muscupng" src="image/muscu.png"> <br>
      
      <img id="f1png" src="image/f1.jpg">
      
      <img id="gamingpng" src="image/gaming.png">
  	</div>

  </div>

  <div id="slide4"> <!-- Slide 4 -->
    
    <div class="slide_inside"> <!-- Pas encore de projets à présenté pour le moment donc on met en standby avec un coming soon -->
      <h2 id="Projets">Projets</h2>
      <img id="comingsoonpng" src="image/coming.jpg">
    </div>

  </div>

  <div id="slide5"> <!-- Slide 5 -->
    
    <div class="slide_inside"> <!-- Tableau 1 ligne sur 2 remplis pour un petit espacement pour combler le vide -->
      <h2 id="Experiences">Experiences</h2>
      <table class="table table-borderless">
    
        <thead> <!-- le haut du tableau avec le nom des colonnes -->
          <tr>
            <th scope="col">Date</th>
            <th scope="col">Lieu/Entreprise</th>
            <th scope="col">Description</th>
          </tr>
        </thead>

        <tbody> <!-- le remplissage du tableau -->
          <tr>
            <td>Juillet 2019 - Septembre 2019</td>
            <td>Veules-Les-Roses / Huitrière du Nordet</td>
            <td>Récolte ostréicole - En pleine mer, il fallait retourner les poches d'huitres et charger celles qui avaient atteint maturités. </td>
          </tr>

          <tr>
            <td></td>
            <td></td>
            <td></td>
          </tr>

          <tr>
            <td>Août 2018</td>
            <td>Dieppe / Forain</td>
            <td>Entretient - Durant la fête foraine de Dieppe, je m'occupais de l'entretient d'un stand en matière de nettoyage et de ravitaillement.</td>
          </tr>

            <tr>
            <td></td>
            <td></td>
            <td></td>
          </tr>

          <tr>
            <td>Novembre 2015</td>
            <td>Torcy-Le-Petit/ SARL Saftair Ventilation</td>
            <td>Stage découverte - Durant ce stage j'ai pendant 1 semaine observé mais également travaillé aux coté du personnel, que ce soit dans l'atelier comme dans les bureaux.</td>
          </tr>

          <tr>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </tbody>
      </table>
    </div>

  </div>

  <div id="slide6"> <!-- Slide 6 -->
    
    <div class="slide_inside"> <!-- Aucune recommendations donc je laisse blanc -->
      <h2 id="Recommandations">Recommandations</h2>
    </div>

  </div>

  <div id="slide7"> <!-- Slide 7 -->
    
    <div class="slide_inside">
      <h2 id="Contact">Contact</h2>
       
       <form name="contact" method="POST" action=""> <!-- Le formulaire de contact -->
         <p class="infoFormulaire">NOM prénom</p>
      
         <p><label><input type="text" name="name" size="50"></label></p>
      
         <p class="infoFormulaire">Email :</p>
      
         <p><label><input type="email" name="email" size="50"></label></p>
      
         <p class="infoFormulaire">Message :</p>
      
         <p><label><textarea name="message" rows="12" cols="100"></textarea></label></p>
      
         <button type="submit" class="btn btn-primary">Envoyer</button>

         <br>

         <img id="goodbyepng" src="image/goodbye.png">
      </form>
      <?php 

      	$host = "localhost";
      	$user = "invite";
      	$password = "Kilimanjaro7845";
      	$db_name = "formulaire";
      	$nom = $_POST["name"];
      	$email = $_POST['email'];
        $message = $_POST['message'];

        $sql = new mysqli($host, $user, $password, $db_name);
                if ($sql->connect_error) {
                    die("Connection failed: ".$sql->connect_error);
                }
                $sql->set_charset('utf8');
                $requete = "INSERT INTO formulaire (nom, email, message) VALUES('$nom', '$email', '$message')";
                if ($sql->query($requete) == TRUE) {
                    echo 'Cest fait ';
                } else {    
                    echo "Error: ".$requete."<br>".$sql->error;
                }
$sql->close();
?>

    </div>




<!-- tout le javascript qui sert à faire tourner la page -->



    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script> <!-- javascript parallax -->
    <script type="text/javascript" src="script/jquery.parallax-1.1.js"></script>
    <script type="text/javascript">	
    	$(document).ready(function(){		
    	    $('#slide1').parallax("center", 0, 0.1, true);
    	    $('#slide2').parallax("center", 900, 0.1, true);
    	    $('#slide3').parallax("center", 2900, 0.1, true);
          $('#slide4').parallax("center", 4900, 0.1, true);
          $('#slide5').parallax("center", 6900, 0.1, true);
          $('#slide6').parallax("center", 8900, 0.1, true);
          $('#slide7').parallax("center", 8900, 0.1, true);

    	})
    </script> 
    <!-- javascript bootstrap -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>